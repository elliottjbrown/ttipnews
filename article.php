<?php   
include("templates/header-html.html");
include("templates/header.html"); 

$articleID = $_GET['id'];		

?>
<ul id="news-article" class="cf"></ul>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script>
$(document).ready(function() {
	//API Call Setup
	var api = '//content.guardianapis.com/'
	var articleID = <?=json_encode($articleID)?>;
	var key = '?show-blocks=all&api-key=5881d598-bf96-45de-9ac5-69c3df430e2c'
	var url = api + articleID + key;
	$.getJSON(url, function(data) {
		
		for (var i =0; i < data.response.total; i++) { 
			
			//API Setup
			var article 	= data.response.content;
			var title 		= data.response.content.webTitle;
			var id 			= data.response.content.id;
			var summary		= data.response.content.blocks.body[0].bodyHtml;
			var figure		= data.response.content.blocks.main.bodyHtml;
			
			// Thumbnail			
			var figureSplit1 	= figure.split('"> ');
			var figure2 		= figureSplit1[1];
			var figureSplit2	= figure2.split('<figcaption>');
			var thumbnail		= figureSplit2[0];			
			//console.log(thumbnail);
			
			// Caption
			var figureSplit3	= figure.split('<figcaption> ');
			var figure3			= figureSplit3[1];
			var figureSplit4	= figure3.split(' <span class="element-image__credit">');
			var caption			= figureSplit4[0];
			//console.log(caption);
			
			// Photo Credit
			var figureSplit5	= figure.split('</span> ');
			var figure4			= figureSplit5[1];
			var figureSplit6	= figure4.split('</figcaption>');
			var photoCredit		= figureSplit6[0] + '</span>';
			//console.log(photoCredit);
			
			var date		= data.response.content.webPublicationDate;
			
			//Change date format
			var dateSplit	= date.split('T');
			var date 		= dateSplit[0];
			var dateSplit 	= date.split("-");
			var yr 			= dateSplit[0][2] + dateSplit[0][3]; //special yr format, take last 2 digits
			var month 		= dateSplit[1];
			if (month == 01) {
				month = "Jan";
			} else if (month == 02) {
				month = "Feb";
			} else if (month == 03) {
				month = "Mar";
			} else if (month == 04) {
				month = "Apr";
			} else if (month == 05) {
				month = "May";
			} else if (month == 06) {
				month = "Jun";
			} else if (month == 07) {
				month = "Jul";
			} else if (month == 08) {
				month = "Aug";
			} else if (month == 09) {
				month = "Sep";
			} else if (month == 10) {
				month = "Oct";
			} else if (month == 11) {
				month = "Nov";
			} else if (month == 12) {
				month = "Dec";
			} else {
				month = "Undefined"
			};
			var day 		= dateSplit[2];
			var date 		= '<div class="date"><p><span class="day">' + day + ' ' + '</span>' + '<span class="month">' + month + ' ' + '</span><span class="year">' + yr + '</span></p></div>';
			
			//Page output
			$('#news-article').append('<li class="sm-12 cls"><div class="inner"><div class="thumbnail"><div class="overlay"></div>' + thumbnail + '<div class="article-text"><h2>' + caption + '</h2></div></div><p class="photoCredit">' + photoCredit + '</p>' + date + '<h2 class="news-article">' + title + '</h2><div class="news-article">' + summary + '</div></div></li>');				
			
		}
	});
});
</script>

<?php   
include("templates/footer.html"); 
	
?>


// find and hide all select boxes with #filter
$("#filter").find("select").hide();

//Map the select boxes and return them as objects
var data = $('#filter select').map(function(){
   var opts = $(this).children().map(function(){
     return{
       text: $(this).text(),
       value: this.value
     }
   }).get()
   var obj={
     id: this.id,
     options:opts   
   }
   return obj;   
}).get();

// Loop through the return object and return data as html
for (var i in data) {
  id = data[i].id;
  var container = "<div id='" + id + "_container' class='_container'><a id='" + id + "_label' class='_label' href='#'>" + id + "</a><div id='" + id + "_dropdown' class='_dropdown hide'></div>";
  $("#filter").append(container);
  for (var opt of data[i].options){  
    options = "<a title="+ this.id +" class='" + this.id + "_option _option' name='" + opt.text + "' value='" + opt.value + "' href='#'>" + opt.text + "</a>";
    $("#"+id+"_dropdown").append(options);
  } 
};

// On click of label show relevant options
$("._label").click(function(){
  $(this).next("._dropdown").toggleClass("hide");
  $("._dropdown").mouseleave(function(){
    $(this).addClass("hide");
  });
});

 

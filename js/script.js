$(document).ready(function() {
	//API Call Setup  	        
    var api = '//content.guardianapis.com/'
    var search = 'search?' 
    var orderBy = 'order-by=newest&'
    var resultsShown = '13'
    var pageSize = 'page-size=' + resultsShown + '&'
    var searchBy = 'q=ttip'
    var key = '&api-key=5881d598-bf96-45de-9ac5-69c3df430e2c'
        
    function newsFeed(data) {
        var url = api + search + orderBy + pageSize + searchBy + '&show-blocks=all' + key;
        $.ajax({
            url: url,
            success: function results(data) {
            // Code that depends on 'data'
            for (var i =0; i < resultsShown; i++) {
                
                var articles 		    = data.response.results;
                //console.log(articles)
                // Title
                try {
                    var title 			= articles[i].webTitle;
                } catch (e) {
                    var title           = undefined;
                }
                
                // ID
                try {
                    var id 				= articles[i].id;
                } catch (e) {
                    var id              = undefined;
                }
               
                // Summary
                try {
                    var summary			= articles[i].blocks.body[0].bodyHtml;
                } catch (e) {
                    var summary         = undefined;
                }
                
                // Photo
                try {
                    var figure			= articles[i].blocks.main.bodyHtml;                          
                    // Thumbnail			
                    var figureSplit1 	= figure.split('"> ');
                    var figure2 		= figureSplit1[1];
                    var figureSplit2	= figure2.split('<figcaption>');
                    var thumbnail		= figureSplit2[0];			
                    //console.log(thumbnail);
                    
                    // Caption
                    var figureSplit3	= figure.split('<figcaption> ');
                    var figure3			= figureSplit3[1];
                    var figureSplit4	= figure3.split(' <span class="element-image__credit">');
                    var caption			= figureSplit4[0];
                    //console.log(caption);
                    
                    // Photo Credit
                    var figureSplit5	= figure.split('</span> ');
                    var figure4			= figureSplit5[1];
                    var figureSplit6	= figure4.split('</figcaption>');
                    var photoCredit		= figureSplit6[0] + '</span>';
                    //console.log(photoCredit);
                
                } catch (e) {
                    var thumbnail = "<img src='images/defaultArticleImage.jpg' alt='TTIP News, the latest from the worlds media' width='1000' height='600'>";
                }         
                
                // Date
                try {
                    var dateRaw	        = articles[i].webPublicationDate;                             
                    //Change date format
                    var dateRawSplit    = dateRaw.split('T');
                    var dateBck         = dateRawSplit[0];
                    var dateSplit2 	    = dateBck.split("-");
                    var yr 			    = dateSplit2[0][2] + dateSplit2[0][3]; //special yr format, take last 2 digits
                    
                    if      (dateSplit2[1] == 01) { month = "Jan"; } 
                    else if (dateSplit2[1] == 02) { month = "Feb"; } 
                    else if (dateSplit2[1] == 03) { month = "Mar"; } 
                    else if (dateSplit2[1] == 04) { month = "Apr"; } 
                    else if (dateSplit2[1] == 05) { month = "May"; } 
                    else if (dateSplit2[1] == 06) { month = "Jun"; } 
                    else if (dateSplit2[1] == 07) { month = "Jul"; } 
                    else if (dateSplit2[1] == 08) { month = "Aug"; } 
                    else if (dateSplit2[1] == 09) { month = "Sep"; } 
                    else if (dateSplit2[1] == 10) { month = "Oct"; } 
                    else if (dateSplit2[1] == 11) { month = "Nov"; } 
                    else if (dateSplit2[1] == 12) { month = "Dec"; } 
                    //else { month = undefined };                
                    
                    var day             = dateSplit2[2];
                    var date            = '<div class="date"><p><span class="day">' + day + ' ' + '</span>' + '<span class="month">' + month + ' ' + '</span><span class="year">' + yr + '</span></p></div>';               
                    
                } catch (e) {
                    var dateRaw         = undefined;
                    var date            = undefined;
                }
                
                // Page output
				
                if (i == 0) { 
                    var content = '<li class="top sm-12 lg-9 mlg-6 cls"><a href="article.php?id='+ id +'" class="news-article"><div class="inner"><div class="overlay"></div>' + date + '<div class="thumbnail">' + thumbnail + '</div><div class="article-text"><h2>' + title + '</h2>' + '<h3>' + caption + '</h3></div></div></a></li>'; 
                } else if (i == 1 || i == 2 || i == 3) {
                    var content = '<li class="middle sm-12 mtp-6 lg-3 mlg-2 cls"><a href="article.php?id='+ id +'" class="news-article"><div class="inner"><div class="overlay"></div>' + date + '<div class="thumbnail">' + thumbnail + '</div><div class="article-text"><h2>' + title + '</h2>' + '<h3>' + caption + '</h3></div></div></a></li>';
                } else if (i == 4) {
                    var content = '<li class="lower-middle sm-12 mtp-6 lg-6 mlg-4 cls"><a href="article.php?id='+ id +'" class="news-article"><div class="inner"><div class="overlay"></div>' + date + '<div class="thumbnail">' + thumbnail + '</div><div class="article-text"><h2>' + title + '</h2>' + '<h3>' + caption + '</h3></div></div></a></li>';
                } else if (i == 5) {
                    var content = '<li class="lower-middle long sm-12 mtp-6 lg-6 mlg-2 cls"><a href="article.php?id='+ id +'" class="news-article"><div class="inner"><div class="overlay"></div>' + date + '<div class="thumbnail">' + thumbnail + '</div><div class="article-text"><h2>' + title + '</h2>' + '<h3>' + caption + '</h3></div></div></a></li>';
                } else if (i == 6 || i == 7 || i == 8) {
                    var content = '<li class="lower long sm-12 mtp-6 lg-6 mlg-4 cls"><a href="article.php?id='+ id +'" class="news-article"><div class="inner"><div class="overlay"></div>' + date + '<div class="thumbnail">' + thumbnail + '</div><div class="article-text"><h2>' + title + '</h2>' + '<h3>' + caption + '</h3></div></div></a></li>';
                } else {
                    var content = '<li class="test lower sm-12 mtp-6 lg-6 mlg-3 cls"><a href="article.php?id='+ id +'" class="news-article"><div class="inner"><div class="overlay"></div>' + date + '<div class="thumbnail">' + thumbnail + '</div><div class="article-text"><h2>' + title + '</h2>' + '<h3>' + caption + '</h3></div></div></a></li>';
                };

                $('#news').append(content);           
                
                }; //end of for loop
                      
            }
        });      
    };
    newsFeed();

    // On click get value of 'clicked' option
    $("._option").click(function(){

        value = $(this).attr("value");
        name = $(this).attr("name"); 
        label = $(this).parent().siblings();
        
        $(label[0]).empty();
        $(label[0]).append(name);
        
        $('#news').empty();

        if ($(this).attr("title") === "orderBy") {
            orderBy = value;
        } else if ($(this).attr("title") === "searchBy") {
            searchBy = value;
        } else {
            alert("This filter is not defined");
        }      
        newsFeed();
    });  
});